#include <iostream> // Include the I/O stream library for input and output
#include <cstdlib>   // Needed for rand() function.
#include <ctime>     // Needed to seed the random number generator with a time value.

// Define a structure for a Player in the game.
struct Player{
    std::string name;  // The name of the player.
    int health;        // The current health of the player.
    int xp;            // Experience points gained by the player. Could be used for leveling up or other game mechanics.
    std::string inventory[10];  // An array of strings for the player's inventory.
    int inventoryCount = 0; 
};

/**
 * @brief Represents a librarian character in our game.
 *
 * The `Librarian` struct captures essential attributes of a librarian,
 * including their name, riddles they present, and answers to those riddles.
 * Additionally, the librarian possesses a collection of ancient books
 * and might have a map to a significant library.
 */
 struct Librarian {
    std::string name;
    std::string riddles[10];
    std::string answers[10];
    std::string ancientBooks[3]; 
    bool hasMapToLibrary; 
};

// Main function, the starting point of the program
int main()
{
    std::cout << "Welcome to the Dragon Realm!" << std::endl;
    std::srand(std::time(0));  // This ensures a different sequence of random numbers each time the program runs.

    // Create an instance of the Player struct
    Player player;
    player.health = 100; // Assign a default value for HP

    // Create an instance of the Librarian struct
    Librarian eldric = {
        "Eldric",
        {
            "What has keys but can't open locks?",
            "The more you take, the more you leave behind. What am I?",
            "What comes once in a minute, twice in a moment, but never in a thousand years?",
            "What has a heart that doesn't beat?",
            "What comes down but never goes up?",
            "I speak without a mouth and hear without ears. What am I?",
            "What has many keys but can't open a single lock?",
            "What is full of holes but still holds water?",
            "What is so fragile that saying its name breaks it?",
            "What gets wet while drying?"
        },
        {
            "Piano",
            "Steps",
            "M",
            "Artichoke",
            "Rain",
            "An echo",
            "A piano",
            "A sponge",
            "Silence",
            "A towel"
        },
        {"The Book of Shadows", "Mystical Creatures", "Time's Passage"},
        true
    };
    
    // Prompt the user to enter their player name
    std::cout << "Please enter your name: ";
    std::cin >> player.name;

    // Display a personalized welcome message to the player with their name
    std::cout << "Welcome " << player.name << " to The Dragon Realm!" << std::endl;

    // Declare an int variable to capture the user's choice
    int choice;
    // Declare an int variable to capture the user's nested choice
    int nestedChoice;
    
    // Initialize a flag to control the loop and signify the player's intent to explore.
    bool exploring = true;
    // As long as the player wishes to keep exploring, this loop will run.
    while(exploring) { 

        // If still exploring, ask the player where they want to go next
        std::cout << "Where will " << player.name << " go next?" << std::endl;
        std::cout << "1. Moonlight Markets" << std::endl;
        std::cout << "2. Grand Library" << std::endl;
        std::cout << "3. Shimmering Lake" << std::endl;
        std::cout << "Please enter your choice: ";
        // Update value of choice
        std::cin >> choice;

        // Respond based on the player's main choice
        switch(choice) {
            //  Handle the Moonlight Markets scenario
            case 1:
                std::cout << "You chose Moonlight Markets." << std::endl;
                break;
            // Handle the Grand Library scenario.
    case 2:
        {
            // Display the setting and introduce Eldric, the librarian.
            std::cout << player.name << " enters the Grand Library, a vast room filled with ancient books, tomes, and scrolls. Standing in the middle is a wise old librarian named Eldric." << std::endl;
            std::cout << "Eldric: Ah, a seeker of knowledge. Answer my riddle correctly, and I will share with you a book from my collection." << std::endl;

            // Choose a random riddle from Eldric's collection for the player to answer.
            int riddleIndex = std::rand() % 5;  // Randomly select one of the 5 riddles.
            std::cout << eldric.name << " says: Answer my riddle! " << eldric.riddles[riddleIndex] << std::endl;
            std::string playerAnswer;  // Stores the player's answer to the riddle.
            std::cin >> playerAnswer;

            // Check if the player's answer matches the correct answer to the riddle.
            if (playerAnswer == eldric.answers[riddleIndex])
            {
                // Reward the player if they answered correctly.
                std::cout << "Correct! Eldric seems pleased." << std::endl;
                player.health += 10;  // Increase player health.
                player.inventory[player.inventoryCount++] = "Book of Shadows";  // Add a book to the player's inventory.
            }
            else
            {
                // Penalize the player for an incorrect answer.
                std::cout << "Incorrect. Eldric seems unhappy." << std::endl;
                player.health -= 10;  // Decrease player health.
            }

            // Check if Eldric has a map and if the player has a chance to receive it.
            if (eldric.hasMapToLibrary && rand() % 100 < 35)  // 35% probability to trigger the following block.
            { 
                // Eldric offers the player a map to the Grand Library.
                std::cout << eldric.name << " whispers, 'You've impressed me.' He discreetly hands you a map of the Grand Library." << std::endl;
                player.inventory[player.inventoryCount++] = "Map of the Grand Library";  // Add the map to the player's inventory.
                eldric.hasMapToLibrary = false;  // Eldric no longer has the map.
            } 

            break;
        }

            // Handle the Shimmering Lake scenario.
            case 3:
                std::cout << player.name << " arrives at Shimmering Lake. It is one of the most beautiful lakes that" << player.name << " has seen. They hear a mysterious melody from the water. They can either: " << std::endl;
                std::cout << "1. Stay quiet and listen" << std::endl;
                std::cout << "2. Sing along with the melody" << std::endl;
                std::cout << "Please enter your choice: ";

                // Capture the user's nested choice
                std::cin >> nestedChoice;

                // If the player chooses to remain silent
                if (nestedChoice == 1)
                {
                    std::cout << "Remaining silent, " << player.name << " hears whispers of the merfolk below, but nothing happens." << std::endl;
                }
                // If the player chooses to sing along with the melody
                else if (nestedChoice == 2)
                {
                    std::cout << "Singing along, a merfolk surfaces and gifts " << player.name
                            << " a special blue gem as a token of appreciation for their voice." 
                            << std::endl;
                    player.inventory[player.inventoryCount] = "Blue Gem";
                    player.inventoryCount++;

                    std::cout << player.name << "'s Inventory:" << std::endl;
                    // Loop through the player's inventory up to the count of items they have
                    for (int i = 0; i < player.inventoryCount; i++)
                    {
                        // Check if the inventory slot is not empty.
                        if (!player.inventory[i].empty())
                        {
                            // Output the item in the inventory slot
                            std::cout << "- " << player.inventory[i] << std::endl;
                        }
                    }

                }
                break;
            // Option to exit the game
            case 4:   
                exploring = false;
                break;
            // If 'choice' is not 1, 2, or 3, this block is executed.
            default:
                std::cout << "You did not enter a valid choice." << std::endl;
                continue; // Errors continue with the next loop iteration
        }
    }

    // Return 0 to indicate successful execution
    return 0;
}
