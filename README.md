# text-adventure

This is the project code for a text-based C++ adventure using [GitLab Duo](https://about.gitlab.com/solutions/code-suggestions/), referenced in the blog post: [Explore the Dragon Realm: Build a C++ adventure game with a little help from AI](https://go.gitlab.com/Bpaq67) .

Code was generated using [GitLab Duo](https://about.gitlab.com/gitlab-duo/)